provider "aws"{
    region = "us-east-1"
}

#use existing VPC
data "aws_vpc" "existing_vpc"{
    id = "vpc-0ad924460cff0b1d3"
}

#use existing subnet
data "aws_subnet" "existing_subnet"{
    vpc_id = data.aws_vpc.existing_vpc.id
    id = "subnet-06656101009ddfde7"
}
#use existing security group

data "aws_security_group" "existing_security_group"{
    id = "sg-022b82f68f9e0f425"
}
